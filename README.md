*README*

1. Clone the repo
 
2. Install node modules
 
3. Install bower components

4. Run Using nodemon


Follow the instruction on confluence
(be more specific with each step)
Steps
1. In client folder, create an index.html as calculator app, create app folder to include the 2 other webpages to be embedded and the various js and css files.

2. In index.html <header>, pull styling from font awesome and bootstrap library into 
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css"/>

3. In index.html <header>, link to local app.css for customised styling
    <link rel="stylesheet" href="/app/app.css"/>

4. In app.css : create the styling for the calculator (centralised, color background, etc)
    .calculator {
    margin-top: 150px;
    border: 5px solid #519d7d;
    background: #333333;
    max-width: 250px;
    }
    .calculator button {
    height: 70px;
    } etc..   

5. In client/app.js, create a new module for the entire app.
    (function () {
    angular
        .module("calculator", []);

    })();

6. In server/app.js: request for the body-parser and express to be call from the server
    var express = require("express");
    var bodyParser = require('body-parser');

7. In server/app.js: set to constant the Node_Port
    const NODE_PORT = process.env.NODE_PORT || 3000;

8. In server/app.js: call the server to initialize the bodyParser.
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());

9. In server/app.js: call the server to serve images, CSS files, and JavaScript files in a directory named client. Be speific in the directory.
    app.use(express.static(__dirname + "/../client/"));

10. In index.html <body>, create a container to contain all the function of the calculator. And identify that main.controller as the Main controller for this page to run.
    <div class="container" ng-controller="MainCtrl as main">

11. In main.controller.js : create the iife to encapsulate the codes so that when the index.html is called on, the codes in there will run.
    (function () {
    angular
        .module("calculator")
        .controller("MainCtrl", MainCtrl);

12. Create and include in home.html and calculator.html into the index.html using ng-include to include both and ng-show to show either one.
    <div ng-include=" '/app/views/home.html' "
         data-ng-show="main.isPageOpen(main.pageType.HOME)"></div>
    <div ng-include=" '/app/views/calculator.html' "
         ng-show="main.isPageOpen(main.pageType.CALCULATOR)"></div>

13. In home.html page : to show the first page of the calculator and in it, create a button with angular function to open the next page - the calculator page
    <div id="page1">
    <i class="fa fa-calculator" aria-hidden="true"></i>
    <br/>
    <a class="btn btn-danger" data-ng-click="main.open(main.pageType.CALCULATOR)"> SHOW CALCULATOR</a>
    </div>

14. In main.controller.js: identity the 2 pages : Home and Calculator and create the function to prompt the html to move to the calculator.html when on click
    function MainCtrl() {
        var self = this;

        self.pageType = {
            HOME: "HOME",
            CALCULATOR: "CALCULATOR"
        };

    }

15. In main.controller.js, under the MainCtrl : initialize the html to open up that the current page to be Home
        self.currentPage = self.pageType.HOME;

16. Create a toggle the true/false between the 2 pages (under the MainCtrl in main.controller.js)
        self.isPageOpen = function (page) {
            return self.currentPage == page;
        };

        self.open = function (page) {
            self.currentPage = page;
        }
17. Remember to enclose everything under the function MainCtrl })();

18. In calculator.html : create the div class, link the controller and bind it
    <div class="calculator container-fluid" ng-controller="CalculatorCtrl as calculator">
    <div class="row">
        <textarea type="text" class="form-control" ng-model="calculator.expression"></textarea>
    </div>

19. In calculator.controller.js: link this controller to the calculator app
    (function () {
    angular
        .module("calculator")
        .controller("CalculatorCtrl", CalculatorCtrl)

20. In calculator.controller.js : create self.expression to capture the numeric data input by user and also to include in the maths symbol
    function CalculatorCtrl() {
        var self = this;

        self.expression = "";

        self.addSymbol = function (symbol) {
            self.expression += symbol
        };

    }
21. In calculator.controller.js, to be included within CalculatorCtrl function : use the eval().tostring(); to calculate the data input and throw it back to the calculator.html page as the result(equate). 
        
        self.equate = function () {
            self.expression = eval(self.expression).toString();
        };

22. Link the "=" symbol (in calculator.html) with the self.equate in the calculator.controller.js

23. Create a back button at the end of the calculator.html to go back to the Home page
        <p class="text-center back-btn">
            <a ng-show="main.isPageOpen(main.pageType.CALCULATOR)" data-ng-click="main.open(main.pageType.HOME)"
            class="btn btn-primary">
            BACK
            </a>
        </p>

24. Include reference to bower components at end of index.html
    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular/angular.min.js"></script>
    <script type="text/javascript" src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

25. And also the various js file in the client side before the end of <body>
    <script type="text/javascript" src="/app/app.js"></script>
    <script type="text/javascript" src="/app/main.controller.js"></script>
    <script type="text/javascript" src="/app/calculator.controller.js"></script>

18. To call the server to be listening and ready to serve the web at all times. No data need to be posted to the server as there is no data to be saved.
    app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
    });
