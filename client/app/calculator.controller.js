(function () {
    angular
        .module("calculator")
        .controller("CalculatorCtrl", CalculatorCtrl)

    function CalculatorCtrl() {
        var self = this;

        self.expression = "";

        self.addSymbol = function (symbol) {
            //self.expression += symbol;
            self.expression =self.expression + symbol;
            console.log(self.expression);
        };

        self.equate = function () {
            self.expression = eval(self.expression).toString();
        };
        //don't use eval in server side. security issues.

    }

})();